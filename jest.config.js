/* eslint-disable @typescript-eslint/tslint/config */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  transform: {
    '^.+\\.tsx?$': 'ts-jest'
  },
  collectCoverage: false,
  coverageDirectory: 'coverage',
  coverageReporters: [
    // "lcov",
    'text'
  ],
  collectCoverageFrom: [
    'src/**/*.ts',
    '!src/**/*.spec.ts',
  ],
  testMatch: [
    '<rootDir>/src/**/*.spec.ts',
  ],
  moduleFileExtensions: [
    'ts',
    'tsx',
    'js',
    'jsx',
    'json'
  ],
  modulePathIgnorePatterns: [
    '<rootDir>/(dist|docs|node_modules)/'
  ],
  transformIgnorePatterns: [
    '<rootDir>/node_modules/',
  ],
  globals: {
    'ts-jest': {
      diagnostics: false,
      isolatedModules: true,
      tsconfig: {
        'target': 'esnext'
      }
    }
  },
  moduleNameMapper: {
    '^unittest$': '<rootDir>/src/unittest',
    '^unittest/(.*)$': '<rootDir>/src/unittest/$1',
  },
  modulePaths: [
    '<rootDir>/src/unittest',
  ]
};
