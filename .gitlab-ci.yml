default:
  image: node:12-alpine
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - node_modules/

  # Validate that the repository contains a package.json and extract a few values from it.
  before_script:
    - |
      if [[ ! -f package.json ]]; then
        echo "No package.json found! A package.json file is required to publish a package to GitLab's NPM registry."
        echo 'For more information, see https://docs.gitlab.com/ee/user/packages/npm_registry/#creating-a-project'
        exit 1
      fi
    - NPM_PACKAGE_NAME=$(node -p "require('./package.json').name")
    - NPM_PACKAGE_VERSION=$(node -p "require('./package.json').version")
    - NPM_PACKAGE_REGISTRY=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/npm/
    - NPM_INSTANCE_LEVEL_ENDPOINT=${CI_API_V4_URL#https?}/packages/npm/
    - NPM_PROJECT_LEVEL_ENDPOINT=${CI_API_V4_URL#https?}/projects/${CI_PROJECT_ID}/packages/npm/
    - npm install -g typescript
    - |
      if [[ -f yarn.lock ]]; then
        apk add yarn
        yarn install --no-progress
      else
        npm install --no-progress
        [[ ! -f package-lock.json ]] && npm prune
      fi

# Validate that the package name is properly scoped to the project's root namespace.
# For more information, see https://docs.gitlab.com/ee/user/packages/npm_registry/#package-naming-convention
validate_package_scope:
  stage: build
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - package.json
  script:
    - |
      if [[ ! $NPM_PACKAGE_NAME =~ ^@$CI_PROJECT_ROOT_NAMESPACE/ ]]; then
        echo "Invalid package scope! Packages must be scoped in the root namespace of the project, e.g. \"@${CI_PROJECT_ROOT_NAMESPACE}/${CI_PROJECT_NAME}\""
        echo 'For more information, see https://docs.gitlab.com/ee/user/packages/npm_registry/#package-naming-convention'
        exit 1
      fi

# If no .npmrc if included in the repo, generate a temporary one to use during the publish step
# that is configured to publish to GitLab's NPM registry
create_npmrc:
  stage: build
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - package.json
  script:
    - |
      if [[ ! -f .npmrc ]]; then
        echo 'No .npmrc found! Creating one now. Please review the following link for more information: https://docs.gitlab.com/ee/user/packages/npm_registry/index.html#authenticating-with-a-ci-job-token'
        touch .npmrc
      fi
      {
        echo unsafe-perm=true
        echo "@${CI_PROJECT_ROOT_NAMESPACE}:registry=${NPM_PACKAGE_REGISTRY}"
        echo "${NPM_INSTANCE_LEVEL_ENDPOINT}:_authToken=\${CI_JOB_TOKEN}"
        echo "${NPM_PROJECT_LEVEL_ENDPOINT}:_authToken=\${CI_JOB_TOKEN}"
      } >> .npmrc
  artifacts:
    paths:
      - .npmrc

# Publish the package. If the version in package.json has not yet been published, it will be
# published to GitLab's NPM registry. If the version already exists, the publish command
# will fail and the existing package will not be updated.
publish_package:
  stage: deploy
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - package.json
  script:
    # Compare the version in package.json to all published versions.
    # If the package.json version has not yet been published, run `npm publish`.
    - |
      if [[ $(npm view "${NPM_PACKAGE_NAME}" versions) == *"'${NPM_PACKAGE_VERSION}'"* ]]; then
        echo "Version ${NPM_PACKAGE_VERSION} of ${NPM_PACKAGE_NAME} has already been published, so no new version has been published."
        exit 0
      fi
      npm publish --loglevel verbose
      echo "Successfully published version ${NPM_PACKAGE_VERSION} of ${NPM_PACKAGE_NAME} to GitLab's NPM registry: ${CI_PROJECT_URL}/-/packages"
  artifacts:
    when: on_failure
    paths:
      - /root/.npm/_logs/
