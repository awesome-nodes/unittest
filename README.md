<h1 align="center">Welcome to Awesome Nodes Unittest Library 👋<br/>
<small></small>
</h1>

<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.2.0-blue.svg?cacheSeconds=2592000" />
  <a href="https://manyfolds.atlassian.net/wiki" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>
  <a href="#" target="_blank">
    <img alt="Code Style: 4-space K&R." src="https://img.shields.io/badge/code%20style-4--space%20K%26R-brightgreen?style=flat" />
  </a>
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-brightgreen.svg" />
  </a>
</p>

> 🔎 Unittest OO framework for converting functional unittest lifecycle hooks into callable object model signatures<br/>
> <small>Provides implementation of reusable test case signatures.</small>

### 🏠 [Homepage][ProjectSite]

### ✨ [Demo][ProjectDemoSite]

## Write Awesome Unittests

1. #### Inject the lifecycle functions of your unittest framework (jest, jasmine, mocha, ...)
    * afterAll
    * afterEach
    * beforeAll
    * beforeEach

   ### Example:
   Create an `index.ts` file located at your unittest configuration directory (usually `src/unittest`)
   and paste the following contents:
   ```typescript
   import { AFTER_ALL_TOKEN, AFTER_EACH_TOKEN, BEFORE_ALL_TOKEN, BEFORE_EACH_TOKEN } from '@awesome-nodes/unittest';
   
   
   AFTER_ALL_TOKEN.Scope.AddProvider({ provide: AFTER_ALL_TOKEN, useValue: afterAll, scope: AFTER_ALL_TOKEN.Scope });
   AFTER_EACH_TOKEN.Scope.AddProvider({ provide: AFTER_EACH_TOKEN, useValue: afterEach, scope: AFTER_EACH_TOKEN.Scope });
   BEFORE_ALL_TOKEN.Scope.AddProvider({ provide: BEFORE_ALL_TOKEN, useValue: beforeAll, scope: BEFORE_ALL_TOKEN.Scope });
   BEFORE_EACH_TOKEN.Scope.AddProvider({ provide: BEFORE_EACH_TOKEN, useValue: beforeEach, scope: BEFORE_EACH_TOKEN.Scope });
   
   export * from '@awesome-nodes/unittest';
   ```
2. Adjust your `tsconfig.json`
   ```
   {
     "compilerOptions": {
       "paths": {
         "unittest": ["src/unittest"],
       }
     }
   }
   ```
3. Write awesome unittests

   `AwesomeTestCase.spec.ts`
    ```typescript
    import { itShould, TestCase } from 'unittest';
    

    class AwesomeTestCase extends TestCase
    {
        Test(): void
        {
            itShould('test things the awesome way', () =>
            {
                // Arrange
           
                // Act
           
                // Assert
            });
        }
   }
   new AwesomeTestCase;
   ```
   A full example is available in [src/unittest/TestCase.spec.ts][UnittestExample].

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing
purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Node.js (Version >= 12) ([Download](https://nodejs.org/en/download/))
* Git (Version >= 2) ([Download](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git))

### Download

```
git clone git+ssh://git@gitlab.com/awesome-nodes/unittest.git
```

### Install

```sh
cd unittest
npm install
```

### Usage
```sh
npm run start
```

### Run Unit Tests
Single Run:
```sh
npm run test
```
Run in watch mode:
```sh
npm run test:w
```

### Run TypeScript Check
Single Run:
```sh
npm run tscheck
```
Run in watch mode:
```sh
npm run tscheck:w
```

### Run Circular Dependency Check
```sh
npm run tscheck:circular
```

### Run Coding Style Tests

```sh
npm run eslint
```

### Run Build
The build output contains three kinds of library variants:
* ES5 for NodeJS applications
* ES6 for tree shaking builds
* and a UMD bundle for web applications
```sh
npm run build
```

## Deployment
> Deployment is done by publishing the npm package using:
```sh
npm publish
```

## Built With
* [BuildSystem](https://gitlab.com/awesome-nodes/build-system) - Integrated Zero Configuration Build System for Awesome Nodes Apps and Framework Components

## 👤 Authors

* **Christian Kusmanow <christian.kusmanow@teslasoft.de>** - *Initial work* - [Teslasoft](https://gitlab.com/teslasoft.de)

See also the list of [contributors](https://gitlab.com/groups/awesome-nodes/-/group_members) who participated in this project.

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check our [issues page](https://gitlab.com/awesome-nodes/build-system/issues).

Please read [CONTRIBUTING.md](./CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

Please read the [DEVELOPING.md](./DEVELOPING.md) for details on how to develop this repository. 

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/awesome-nodes/build-system/-/tags). 

## Show your support

Give a ⭐️ if this project helped you!

## License

[MIT](./LICENSE)

***
_This README was generated initially with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_

[ProjectSite]: https://gitlab.com/awesome-nodes/unittest

[ProjectDemoSite]: https://unittest.demo.awesome-nodes.com

[UnittestExample]: ./src/unittest/TestCase.spec.ts
