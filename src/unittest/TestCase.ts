import { InjectionToken } from '@awesome-nodes/injection-factory';
import { ArgumentException, ObjectBase, TemplateString } from '@awesome-nodes/object';
import { Nullable, PlainObject } from 'simplytyped';
import { ITestCase, TestEvent } from 'unittest/ITestCase';
import { DoneCallback, FunctionLike, Lifecycle } from 'unittest/Lifecycle';
import { TestLifecycleHooks } from 'unittest/TestLifecycleHooks';
import { AFTER_ALL_TOKEN, AFTER_EACH_TOKEN, BEFORE_ALL_TOKEN, BEFORE_EACH_TOKEN } from 'unittest/TestMacros';
import ProvidesCallback = jest.ProvidesCallback;


/**
 * Represents the jest unit test suite task runner and integrates the unit test environment into the object model.
 * Provides ability to create derivable test implementations for clean inheritable and scalable test case definitions.
 */
export abstract class TestCase extends ObjectBase implements ITestCase
{
    public constructor(name?: string)
    {
        super(name);
        this.Init();
    }

    /**
     * Initializes this test case instance and registers all test lifecycle hooks.
     */
    protected Init(): void
    {
        this.LoadDefaultLifecycleMacro(BEFORE_ALL_TOKEN, beforeAll);
        this.LoadDefaultLifecycleMacro(BEFORE_EACH_TOKEN, beforeEach);
        this.LoadDefaultLifecycleMacro(AFTER_EACH_TOKEN, afterAll);
        this.LoadDefaultLifecycleMacro(AFTER_ALL_TOKEN, afterEach);

        describe(this.Name, () =>
        {
            this.Describe && this.Describe();
            this.InitLifecycleHooks();
            this.Test();
        });
    }

    protected LoadDefaultLifecycleMacro(token: InjectionToken<unknown>, lifecycleMacro: FunctionLike): void
    {
        if (!token.Scope.Providers.some(_provider => InjectionToken.Compare(_provider.Token, token)))
            token.Scope.AddProvider({ provide: token, useValue: lifecycleMacro, scope: token.Scope });
    }

    //region Lifecycle Hook Registration

    /**
     * Override to add more default lifecycle hooks or assertions during test suite initialization describe.
     */
    protected Describe?(): void;

    /**
     * Links the defined unit test lifecycle elements to the jest test description framework.
     */
    protected InitLifecycleHooks(): void
    {
        this.ImplementsLifecycle(TestLifecycleHooks.BeforeAll) &&
        this.RegisterLifecycleHook(TestLifecycleHooks.BeforeAll);

        this.ImplementsLifecycle(TestLifecycleHooks.BeforeEach) &&
        this.RegisterLifecycleHook(TestLifecycleHooks.BeforeEach);

        this.ImplementsLifecycle(TestLifecycleHooks.AfterEach) &&
        this.RegisterLifecycleHook(TestLifecycleHooks.AfterEach);

        this.ImplementsLifecycle(TestLifecycleHooks.AfterAll) &&
        this.RegisterLifecycleHook(TestLifecycleHooks.AfterAll);
    }

    /**
     * Registers a jest test suite lifecycle hook for the provided test event or done callback.
     * The hook handler function gets bound to the this instance context.
     * @param {TestLifecycleHooks} lifecycle The lifecycle hook event handler member function of this test case.
     * @param {ProvidesCallback} lifecycleHook If specified it is used instead of the lifecycle hook implementation
     * from this type ({@link BeforeAll}, {@link BeforeEach}, {@link AfterEach}, {@link AfterAll}).
     */
    protected RegisterLifecycleHook(
        lifecycle: TestLifecycleHooks,
        lifecycleHook?: ProvidesCallback): void
    {
        let registerFunction: Nullable<Lifecycle> = null;
        let defaultHook: Nullable<TestEvent>      = null;

        switch (lifecycle) {
            case TestLifecycleHooks.BeforeAll:
                registerFunction = BEFORE_ALL_TOKEN.Value;
                defaultHook      = this.BeforeAll;
                break;
            case TestLifecycleHooks.BeforeEach:
                registerFunction = BEFORE_EACH_TOKEN.Value;
                defaultHook      = this.BeforeEach;
                break;
            case TestLifecycleHooks.AfterEach:
                registerFunction = AFTER_EACH_TOKEN.Value;
                defaultHook      = this.AfterEach;
                break;
            case TestLifecycleHooks.AfterAll:
                registerFunction = AFTER_ALL_TOKEN.Value;
                defaultHook      = this.AfterAll;
                break;
            default:
                throw new ArgumentException(`Unknown lifecycle hook '${lifecycle}'.`);
        }

        const testEvent = lifecycleHook == null && this.ImplementsLifecycle(lifecycle)
            ? (this as PlainObject)[lifecycle.Value]
            : lifecycleHook;

        // Break out on invalid object state or implementation failure.
        if (testEvent == null && defaultHook == null)
            throw new ArgumentException(
                TemplateString`A default test lifecycle hook event handler for lifecycle '${
                    'paramName'
                }' is required.`,
                'lifecycle',
            );

        // If no lifecycle event has been specified the default lifecycle handler of this type will be used.
        const hook = (testEvent == null && lifecycleHook == null ? defaultHook : testEvent);

        // Register the lifecycle hook and ensure that the handler
        // function gets always invoked on this instance context.
        registerFunction(hook.bind(this));
    }

    /**
     * Determines if the calling instance prototype implements the specified lifecycle hook.
     * @param {string} lifecycleName
     * @returns {boolean}
     */
    protected ImplementsLifecycle(lifecycleName: TestLifecycleHooks): boolean
    {
        return (lifecycleName.Value in this) && typeof (this as PlainObject)[lifecycleName.Value] === 'function';
    }

    /**
     * Executes the provided callable function asynchronously using a provided timout delay in milliseconds.
     * @param {Function} callback The callable function to be executed asynchronously.
     * @param {number} ms The delay in milliseconds
     * @returns {ProvidesCallback}
     */
    protected Timeout(callback: CallableFunction, ms = 0): ProvidesCallback
    {
        return (done: DoneCallback) =>
        {
            setTimeout(
                () =>
                {
                    callback();
                    done();
                },
                ms,
            );
        };
    }

    //endregion

    //region ITest Members

    /** @inheritDoc */
    public BeforeAll?(callback?: DoneCallback): void;

    /** @inheritDoc */
    public BeforeEach?(callback?: DoneCallback): void;

    /** @inheritDoc */
    public AfterAll?(callback?: DoneCallback): void;

    /** @inheritDoc */
    public AfterEach?(callback?: DoneCallback): void;

    /** @inheritDoc */
    public abstract Test(): void;

    //endregion
}
