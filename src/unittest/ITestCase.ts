import { DoneCallback } from 'unittest/Lifecycle';


export interface TestEvent
{
    (callback: DoneCallback, ...args: Array<unknown>): void;
}

/**
 * Defines a default unit test object with lifecycle elements.
 */
export interface ITestCase
{
    /**
     * Override this method to provide a {@link beforeAll} hook which runs before all tests in this block.
     */
    BeforeAll?(callback: DoneCallback, ...args: Array<unknown>): void;

    /**
     * Override this method to provide a default {@link beforeEach} hook which runs before each test in this block.
     */
    BeforeEach?(callback: DoneCallback, ...args: Array<unknown>): void;

    /**
     * Override this method to provide a default {@link afterAll} hook which runs after tests in this block.
     */
    AfterAll?(callback: DoneCallback, ...args: Array<unknown>): void;

    /**
     * Override this method to provide a default {@link afterEach} hook which runs after all tests in this block.
     */
    AfterEach?(callback: DoneCallback, ...args: Array<unknown>): void;

    /**
     * Implements the initialization of all assertions in this describe context block.
     */
    Test(): void;
}
