import { Enum } from '@awesome-nodes/object';


/**
 * Defines the default unit test lifecycle hooks.
 */
export class TestLifecycleHooks extends Enum<string>
{
    public static readonly BeforeAll: TestLifecycleHooks  = TestLifecycleHooks.Enum('BeforeAll');
    public static readonly BeforeEach: TestLifecycleHooks = TestLifecycleHooks.Enum('BeforeEach');
    public static readonly AfterEach: TestLifecycleHooks  = TestLifecycleHooks.Enum('AfterEach');
    public static readonly AfterAll: TestLifecycleHooks   = TestLifecycleHooks.Enum('AfterAll');
}
