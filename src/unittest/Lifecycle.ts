export interface FunctionLike
{
    readonly name: string;
}

export interface DoneCallback
{
    (...args: Array<unknown>): unknown;

    fail(error?: string | { message: string }): unknown;
}

export type Lifecycle = (fn: jest.ProvidesCallback, timeout?: number) => unknown;
