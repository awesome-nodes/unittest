import { itShould, TestLifecycleHooks } from 'unittest';
import { TestCase } from 'unittest/TestCase';


/**
 * Plain unit test
 */
describe('UnitTestExample', () =>
{
    describe('UnitTest', () =>
    {
        beforeEach((done: jest.DoneCallback) =>
        {
            done();
        });

        it('should expect something', () =>
        {
            expect.anything();
        });
    });
});

/**
 * Tests test case scoping is working as expected.
 */
/* tslint:disable:no-unused-expression */
new class TestCaseExample extends TestCase
{
    public Test(): void
    {
        /**
         * Demonstrates the inheritance capabilities of AngularJSTestCase
         */
        class LifecycleTest extends TestCase
        {
            public describe!: string;
            public before!: string;
            public manualBefore!: boolean;
            public beforeEach!: string;
            public manualBeforeEach!: boolean;
            public afterEach!: string;
            public manualAfterEach!: boolean;
            public after!: string;
            public manualAfter!: true;

            /** @inheritDoc */
            protected Describe(): void
            {
                this.describe = 'DescribeValue';

                // Use untillity functions to generate hooks dynamically
                this.RegisterLifecycleHook(
                    TestLifecycleHooks.BeforeAll,
                    this.Timeout(() => this.manualBefore = true),
                );
                this.RegisterLifecycleHook(
                    TestLifecycleHooks.AfterAll,
                    this.Timeout(() => this.manualAfter = true),
                );

                this.RegisterLifecycleHook(
                    TestLifecycleHooks.BeforeEach,
                    this.Timeout(() => this.manualBeforeEach = true),
                );
                this.RegisterLifecycleHook(
                    TestLifecycleHooks.AfterEach,
                    this.Timeout(() => this.manualAfterEach = true),
                );
            }

            public BeforeAll(done: jest.DoneCallback): void
            {
                this.Timeout(() => this.before = 'BeforeValue')(done);
            }

            /** @inheritDoc */
            public BeforeEach()
            {
                this.beforeEach = 'BeforeEachValue';
            }

            /** @inheritDoc */
            public AfterEach()
            {
                this.afterEach = 'AfterEachValue';
            }

            /** @inheritDoc */
            public AfterAll(done: jest.DoneCallback): void
            {
                this.Timeout(() => this.after = 'AfterValue')(done);
            }

            //noinspection JSUnusedGlobalSymbols
            /** @inheritDoc */
            public Test()
            {
                itShould('access all lifecycle generated test values', () =>
                {
                    expect(this.describe).toBe('DescribeValue');

                    expect(this.before).toBe('BeforeValue');
                    expect(this.manualBefore).toBe(true);

                    expect(this.beforeEach).toBe('BeforeEachValue');
                    expect(this.manualBeforeEach).toBe(true);
                });
            }
        }

        /**
         * Demonstrates the referencibility of executed test cases for further analysis.
         */
        new class LifecycleTestResult extends TestCase
        {
            constructor(private _LifecycleTest: LifecycleTest)
            {
                super();
            }

            public Test(): void
            {
                itShould('access all lifecycle generated test values', () =>
                {
                    expect(this._LifecycleTest.afterEach).toBe('AfterEachValue');
                    expect(this._LifecycleTest.manualAfterEach).toBe(true);

                    expect(this._LifecycleTest.after).toBe('AfterValue');
                    expect(this._LifecycleTest.manualAfter).toBe(true);
                });
            }
        }(new LifecycleTest());
    }
}();
