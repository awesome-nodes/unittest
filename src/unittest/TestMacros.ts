import { InjectionScope, InjectionToken } from '@awesome-nodes/injection-factory';
import { Lifecycle } from 'unittest/Lifecycle';
import { TestLifecycleHooks } from 'unittest/TestLifecycleHooks';
import ProvidesCallback = jest.ProvidesCallback;


export const itShould = (
    text: string,
    callback: ProvidesCallback): void =>
    it(`should ${text}`, callback);

export const itReturns = (
    condition: number | string | boolean,
    calledWith: string,
    callback: ProvidesCallback): void =>
    it(`should return ${condition} when called with '${calledWith}'`, callback);

const TestCaseScope = InjectionScope.get(TestLifecycleHooks, null);

export const BEFORE_ALL_TOKEN: InjectionToken<Lifecycle> = TestCaseScope.AddToken(TestLifecycleHooks.BeforeAll);

export const BEFORE_EACH_TOKEN: InjectionToken<Lifecycle> = TestCaseScope.AddToken(TestLifecycleHooks.BeforeEach);

export const AFTER_EACH_TOKEN: InjectionToken<Lifecycle> = TestCaseScope.AddToken(TestLifecycleHooks.AfterEach);

export const AFTER_ALL_TOKEN: InjectionToken<Lifecycle> = TestCaseScope.AddToken(TestLifecycleHooks.AfterAll);
