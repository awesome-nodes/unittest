# Awesome Nodes Unittest Helper Library

<p>
  <a href="#" target="_blank">
    <img alt="Code Style: 4-space K&R." src="https://img.shields.io/badge/code%20style-4--space%20K%26R-brightgreen?style=flat" />
  </a>
</p>

Please read our [Code of Conduct](./CONTRIBUTING.md) for understanding on how to contribute to this project.

During development you can use the following utilities:
* **TypeScript Type Checking**
  ```sh
  npm run test:w
  ```
* ***Jest* Test Environment**
  ```sh
  npm run test:w
  ```
* 
